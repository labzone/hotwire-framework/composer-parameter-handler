<?php

namespace HotWire\ParameterHandler;

use Composer\Script\Event;
use Symfony\Component\Yaml\Parser;

class Handler
{
    const PARAMETER="hotwire-parameters";

    public static function build(Event $event)
    {
        $extras = $event->getComposer()->getPackage()->getExtra();
        if (!isset($extras[self::PARAMETER])) {
            throw new \InvalidArgumentException('The parameter handler needs to be configured through the extra.hotwire-parameters setting.');
        }
        $configs = $extras[self::PARAMETER];
        //print_r($event->getIO());
        $parser = new Parser();
        $parsed = $parser->parse(file_get_contents($configs['file']));

        print_r($parsed);
    }
}